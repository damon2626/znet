package org.zstacks.znet.log.impl;

import org.zstacks.znet.log.Logger;
import org.zstacks.znet.log.LoggerFactory;
 

public class JdkLoggerFactory implements LoggerFactory {
	
	public Logger getLogger(Class<?> clazz) {
		return new JdkLogger(clazz);
	}
	
	public Logger getLogger(String name) {
		return new JdkLogger(name);
	}
}
